set nocompatible

" See https://github.com/junegunn/vim-plug/wiki/tutorial
call plug#begin('~/.vim/plugged')

" Tabular aligns things. See http://vimcasts.org/episodes/aligning-text-with-tabular-vim/
Plug 'godlygeek/tabular'

" Asynchronous linting/fixing for Vim and Language Server Protocol (LSP) integration
Plug 'w0rp/ale'

" lean & mean status/tabline for vim that's light as air
Plug 'bling/vim-airline'

" A Git wrapper so awesome, it should be illegal
Plug 'tpope/vim-fugitive'

" A Vim plugin which shows a git diff in the gutter (sign column) and stages/undoes hunks.
Plug 'airblade/vim-gitgutter'

" a class outline viewer for Vim
Plug 'majutsushi/tagbar'
nnoremap <silent> <F9> :TagbarToggle<CR>

" extensible and asynchronous completion framework
Plug 'Shougo/deoplete.nvim'
Plug 'roxma/nvim-yarp'
Plug 'roxma/vim-hug-neovim-rpc'
let g:deoplete#enable_at_startup = 1

" This plugin adds Go language support for Vim
Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }
" Go {{{
au FileType go nmap <leader>r <Plug>(go-run)
au FileType go nmap <leader>b <Plug>(go-build)
au FileType go nmap <leader>t <Plug>(go-test)
au FileType go nmap <leader>c <Plug>(go-coverage)

au FileType go nmap <Leader>ds <Plug>(go-def-split)
au FileType go nmap <Leader>dv <Plug>(go-def-vertical)
au FileType go nmap <Leader>dt <Plug>(go-def-tab)

au FileType go nmap <Leader>gd <Plug>(go-doc)
au FileType go nmap <Leader>gv <Plug>(go-doc-vertical)

au FileType go set ts=4

" }}}

" Surround selection with brackets
Plug 'tpope/vim-surround'

" Fuzzy file name matching (also buffers, etc.)
Plug 'ctrlpvim/ctrlp.vim'

" Vastly improved Javascript indentation and syntax support in Vim.
Plug 'pangloss/vim-javascript'

call plug#end()

filetype plugin indent on

let mapleader = "-"
" let localmapleader = "\\"

" Copy all yanks to the unnamed clipboard
set clipboard=unnamed

syntax enable
"set expandtab autoindent smartindent
set softtabstop=4
set shiftwidth=4
" set tabstop=8
set expandtab
set nrformats-=octal

set laststatus=2
set noshowmode
set showcmd

set nobackup
set backupdir^=~/.vim/backup//
set directory^=~/.vim/tmp//
set undodir^=~/.vim/tmp//
set undofile
set history=50

" search behaviour
set incsearch
set hlsearch
nnoremap <leader>/ :nohlsearch<cr>
set ignorecase
set smartcase
" use standard regex -- breaks 'repeat' search
" nnoremap / /\v
" vnoremap / /\v

" UI stuff
colorscheme colorsbox-stbright
if has('gui_running')
    set guifont=Monaco:h12,PowerlineSymbols:h12
    "set guifont=Source\ Code\ Pro:h12,Monaco:h12,PowerlineSymbols:h12
else
    " highlight ColorColumn ctermbg=8
endif
set colorcolumn=+0
set nocursorline
set visualbell
set wildmenu
set scrolloff=3
set nowrap

" line numbering
set relativenumber
set number

" edit and source vimrc easily
noremap <leader>ev :split $MYVIMRC<cr>
noremap <leader>sv :source $MYVIMRC<cr>

" JavaScript {{{
" This requires the vim-javascript plugin
augroup javascript_folding
    au!
    au FileType javascript setlocal foldmethod=syntax
    set tabstop=2 expandtab shiftwidth=2
augroup END
" }}}

" JSON file settings {{{
augroup json_vim
	autocmd!
	autocmd BufNewFile,BufRead *.json set ft=javascript
augroup END
" }}}

" Vimscript file settings {{{
augroup filetype_vim
    autocmd!
    autocmd FileType vim setlocal foldmethod=marker
augroup END
" }}}

" Markdown file type {{{
augroup mv_markdown
	autocmd!
    " manipulate markdown header of current section
    autocmd FileType markdown onoremap ih :<c-u>execute "normal! ?^==\\+\\\|--\\+$\r:nohlsearch\rkvg_"<cr>
    autocmd FileType markdown onoremap ah :<c-u>execute "normal! ?^==\\+\\\|--\\+$\r:nohlsearch\rg_vk0"<cr>
augroup end
" }}}

" YAML file type {{{
augroup mv_yaml
    autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab
augroup end
" }}}

" Cscope {{{
if has("cscope")
    "set csprg=/usr/local/bin/cscope
    set csto=0
    set cst
    set nocsverb
    " add any database in current directory
    if filereadable("cscope.out")
	cs add cscope.out
    " else add database pointed to by environment
    elseif $CSCOPE_DB != ""
	cs add $CSCOPE_DB
    endif
    set csverb

   " key bindings 
    nmap <C-_>s :cs find s <C-R>=expand("<cword>")<CR><CR>
    nmap <C-_>g :cs find g <C-R>=expand("<cword>")<CR><CR>
    nmap <C-_>c :cs find c <C-R>=expand("<cword>")<CR><CR>
    nmap <C-_>t :cs find t <C-R>=expand("<cword>")<CR><CR>
    nmap <C-_>e :cs find e <C-R>=expand("<cword>")<CR><CR>
    nmap <C-_>f :cs find f <C-R>=expand("<cfile>")<CR><CR>
    nmap <C-_>i :cs find i ^<C-R>=expand("<cfile>")<CR>$<CR>
    nmap <C-_>d :cs find d <C-R>=expand("<cword>")<CR><CR>

    " Using 'CTRL-spacebar' then a search type makes the vim window
    " split horizontally, with search result displayed in
    " the new window.

    nmap <C-Space>s :scs find s <C-R>=expand("<cword>")<CR><CR>
    nmap <C-Space>g :scs find g <C-R>=expand("<cword>")<CR><CR>
    nmap <C-Space>c :scs find c <C-R>=expand("<cword>")<CR><CR>
    nmap <C-Space>t :scs find t <C-R>=expand("<cword>")<CR><CR>
    nmap <C-Space>e :scs find e <C-R>=expand("<cword>")<CR><CR>
    nmap <C-Space>f :scs find f <C-R>=expand("<cfile>")<CR><CR>
    nmap <C-Space>i :scs find i ^<C-R>=expand("<cfile>")<CR>$<CR>
    nmap <C-Space>d :scs find d <C-R>=expand("<cword>")<CR><CR>

    " Hitting CTRL-space *twice* before the search type does a vertical
    " split instead of a horizontal one

    nmap <C-Space><C-Space>s
	    \:vert scs find s <C-R>=expand("<cword>")<CR><CR>
    nmap <C-Space><C-Space>g
	    \:vert scs find g <C-R>=expand("<cword>")<CR><CR>
    nmap <C-Space><C-Space>c
	    \:vert scs find c <C-R>=expand("<cword>")<CR><CR>
    nmap <C-Space><C-Space>t
	    \:vert scs find t <C-R>=expand("<cword>")<CR><CR>
    nmap <C-Space><C-Space>e
	    \:vert scs find e <C-R>=expand("<cword>")<CR><CR>
    nmap <C-Space><C-Space>i
	    \:vert scs find i ^<C-R>=expand("<cfile>")<CR>$<CR>
    nmap <C-Space><C-Space>d
	    \:vert scs find d <C-R>=expand("<cword>")<CR><CR>
endif
" }}}

nnoremap ; :

inoremap jk <esc>
":inoremap <esc> <nop>

" move line down and up
noremap <leader>- ddp
noremap <leader>_ ddkP

" select current word, and uppercase
inoremap <leader><c-u> <esc>viwUi
nnoremap <leader><c-u> viwU

" single or double-quote the current word or visual selection
" The h is there to ensure e works correctly when on the last character
nnoremap <leader>" hea"<esc>hbi"<esc>lel
nnoremap <leader>' hea'<esc>hbi'<esc>lel

vnoremap <leader>" <esc>`>a"<esc>`<i"<esc>`>ll
vnoremap <leader>' <esc>`>a'<esc>`<i'<esc>`>ll

" handy abbreviations
iabbrev @n Martien Verbruggen
iabbrev @@ martien.verbruggen@gmail.com
iabbrev @w mverbruggen@atlassian.com

" typos, typos
iabbrev adn and
iabbrev firend friend
iabbrev waht what
iabbrev tehn then
" iabbrev filed field

" block comments
iabbrev #b /****************************************
iabbrev #e <Space>****************************************/
