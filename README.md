My vim configuration
====================

This config uses pathogen to install a bundle of things I find useful.

How to use
----------

To use, move your ~/.vimrc file and ~/.vim directory out of the way. Then:`

  cd ~
  git clone <this-repo-url> .vim
  cd .vim
  git submodule update --init

You should probably read up on how to manage submodules in git. If you want to
update one of the submodules, you just need to cd into that directory, and run
a git pull:

  cd ~/.vim/bundle/vim-BUNDLE
  git pull origin master

or you can do them all with this bit of submodule magic:

  cd ~/.vim
  git submodule foreach git pull origin master

IdeaVim
--------
If you also use ideavim for IntelliJ IDEA, create a symlink to the ideavimrc.vim
file in your home directory

  cd ~
  ln -s .vim/ideavimrc.vim .ideavimrc

