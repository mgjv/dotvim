" Find next or previous help "link"
nnoremap <buffer> <leader>] /\|.\{-}\|<Return>:nohl<Return>
nnoremap <buffer> <leader>[ ?\|.\{-}\|<Return>:nohl<Return>

" For some reason, the :keepjumps results in the cursor being positioned on
" the first non-blank char of the line with the link, instead of on the link
"nnoremap <buffer> <A-Tab> :keepjumps /\|.\{-}\|<Return>
"nnoremap <buffer> <S-Tab> :keepjumps ?\|.\{-}\|<Return>

